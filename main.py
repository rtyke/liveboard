import csv
import logging
import sys
import time
from datetime import datetime

import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from timing import elapsed_timer

ban_message = 'Internal Server Error'
waiting_time = 10


def stp():
    import sys
    sys.exit()

def save_to_file(filename, data):
    f = open(filename, 'w')
    f.write(data)
    f.close()

def boil_soup(url):
    r = requests.get(url, proxies=get_http_proxy(), verify=False)
    if r.status_code != 200:
        sys.exit('request code fur ulr ' + url + ' is ' + str(r.status_code))
    content = r.text
    # logger.debug("Link text %s" % r.text)

    soup = bs(content, 'html.parser')
    if soup.body.text.find(ban_message) != -1:
        raise Exception("Banned!")
    return soup

def get_date(date_container):
    date_from_container = date_container.text
    data_str = date_from_container[:-3] + ' ' + date_from_container[-3:] + ' 2016'
    return datetime.strptime(data_str, '%d %b %Y')

def get_vessel_id(soup):
    id_container = soup.find(id='hdfboatId')
    id = id_container['value']
    return id

def generate_iti_href(vessel_id):
    return 'http://www.liveaboard.com/pages/BoatResultsControlHost.aspx?boatid=' + vessel_id

def get_vessel_name(soup):
    return soup.title.string.strip()


def tmstmp():
    return datetime.now().strftime('%Y-%m-%d')

def get_http_proxy():
    local_proxy = '127.0.0.1:8118'
    http_proxy = {  'http': local_proxy,
                    'https': local_proxy}
    return http_proxy

def scrape_iti_data(soup):
    iti_dics = []

    lines = soup.find_all('tr', 'melinky')

    for line in lines:
        iti_dic = {'iti_name': '',
                    'start_date' : '',
                    'return_date' : '',
                    'notes' : '',
                    'price': '',
                    'disc_price' : '',
                    'disc' : '',
                    'availability' : ''
            }
        cells = line.find_all('td')
        date_td = cells[0]
        iti_dic['start_date'] = get_date(date_td)
        iti_dic['return_date'] = cells[1].div.span.text
        iti_dic['iti_name'] = cells[1].a.text
        if len(cells[1].contents) == 2:
            highlight = cells[1].contents[-1].text
            if highlight.find('OFF') != -1:
                iti_dic['disc'] = highlight
            else:
                iti_dic['notes'] = highlight

        price_cells = cells[2].div.contents
        price_values = list(map(lambda x: x.string, price_cells))

        # define if there is discount
        if price_values[1]:
            iti_dic['price'] = price_values[1]
            iti_dic['disc_price'] = price_values[3]
            iti_dic['disc'] = price_values[-1]
        else:
            iti_dic['price'] = price_values[3]
        iti_dic['availability'] = cells[-1].text.replace('Select Cabin', '').strip()
        iti_dics.append(iti_dic)
    return iti_dics

def get_my_ip():
    url = 'http://ipinfo.io/ip'
    r = requests.get(url, proxies=get_http_proxy(), verify=False)
    return r.text.strip()


vessels = [
    ('http://www.liveaboard.com/scubaspa-yang', 'USD'),
    ('http://www.liveaboard.com/la-galigo', 'USD'),
    ('http://www.liveaboard.com/aurora-liveaboard', 'USD'),
    ('http://www.liveaboard.com/okeanos-aggressor', 'USD'),
    ('http://www.liveaboard.com/galapagos-sky', 'USD'),
    ('http://www.liveaboard.com/galapagos-master', 'USD'),
    ('http://www.liveaboard.com/indo-aggressor' , 'USD'),
    ('http://www.liveaboard.com/humboldt-explorer' , 'USD'),
    ('http://www.liveaboard.com/fiji-siren' , 'EUR'),
    ( 'http://www.liveaboard.com/nortada-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/argo-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/palau-siren' , 'EUR'),
    ( 'http://www.liveaboard.com/mv-samambaia' , 'USD'),
    ( 'http://www.liveaboard.com/waow-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/rock-islands-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/euphoria-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/tiger-blue-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/palau-aggressor-ii' , 'USD'),
    ( 'http://www.liveaboard.com/orion-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/mermaid-ii' , 'EUR'),
    ( 'http://www.liveaboard.com/raja-ampat-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/ombak-putih' , 'USD'),
    ( 'http://www.liveaboard.com/carpe-vita-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/solitude-one' , 'USD'),
    ( 'http://www.liveaboard.com/philippine-siren' , 'EUR'),
    ( 'http://www.liveaboard.com/scubaspa-ying' , 'USD'),
    ( 'http://www.liveaboard.com/emperor-serenity' , 'USD'),
    ( 'http://www.liveaboard.com/tambora-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/alila-purnama' , 'USD'),
    ( 'http://www.liveaboard.com/belize-aggressor-iv' , 'USD'),
    ( 'http://www.liveaboard.com/sea-safari-vi' , 'USD'),
    ( 'http://www.liveaboard.com/avalon-ii' , 'USD'),
    ( 'http://www.liveaboard.com/bilikiki' , 'USD'),
    ( 'http://www.liveaboard.com/truk-master' , 'USD'),
    ( 'http://www.liveaboard.com/cayman-aggressor-iv' , 'USD'),
    ( 'http://www.liveaboard.com/kona-aggressor-ii' , 'USD'),
    ( 'http://www.liveaboard.com/belize-aggressor-iii' , 'USD'),
    ( 'http://www.liveaboard.com/indo-siren' , 'EUR'),
    ( 'http://www.liveaboard.com/cheng-ho-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/adelaar-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/ambai-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/seven-seas-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/sea-safari-8' , 'USD'),
    ( 'http://www.liveaboard.com/ocean-hunter-1' , 'USD'),
    ( 'http://www.liveaboard.com/carpe-novo-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/virgo-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/emperor-voyager' , 'USD'),
    ( 'http://www.liveaboard.com/leo-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/dewi-nusantara' , 'USD'),
    ( 'http://www.liveaboard.com/galatea-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/ocean-divine' , 'USD'),
    ( 'http://www.liveaboard.com/amba-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/valentina' , 'USD'),
    ( 'http://www.liveaboard.com/astrea-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/red-sea-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/fiji-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/okeanos-aggressor-ii' , 'USD'),
    ( 'http://www.liveaboard.com/thorfinn' , 'USD'),
    ( 'http://www.liveaboard.com/avalon-i' , 'USD'),
    ( 'http://www.liveaboard.com/arenui' , 'USD'),
    ( 'http://www.liveaboard.com/nautilus-under-sea' , 'USD'),
    ( 'http://www.liveaboard.com/nautilus-explorer' , 'USD'),
    ( 'http://www.liveaboard.com/ondina' , 'EUR'),
    ( 'http://www.liveaboard.com/matahariku' , 'EUR'),
    ( 'http://www.liveaboard.com/nautilus-belle-amie' , 'USD'),
    ( 'http://www.liveaboard.com/tortuga' , 'USD'),
    ( 'http://www.liveaboard.com/bahamas-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/diva-andaman' , 'USD'),
    ( 'http://www.liveaboard.com/turks-and-caicos-aggressor-ii' , 'USD'),
    ( 'http://www.liveaboard.com/spoilsport-liveaboard' , 'AUD'),
    ( 'http://www.liveaboard.com/ocean-hunter-3' , 'USD'),
    ( 'http://www.liveaboard.com/taka-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/carpe-diem-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/honors-legacy' , 'USD'),
    ( 'http://www.liveaboard.com/calico-jack' , 'EUR'),
    ( 'http://www.liveaboard.com/katharina-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/mv-chertan' , 'USD'),
    ( 'http://www.liveaboard.com/turks-and-caicos-explorer' , 'USD'),
    ( 'http://www.liveaboard.com/seadoors' , 'EUR'),
    ( 'http://www.liveaboard.com/la-reina' , 'USD'),
    ( 'http://www.liveaboard.com/quino-el-guardian' , 'USD'),
    ( 'http://www.liveaboard.com/blue-force-one' , 'USD'),
    ( 'http://www.liveaboard.com/sri-lanka-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/blue-voyager' , 'GBP'),
    ( 'http://www.liveaboard.com/manta-cruise-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/emperor-atoll' , 'USD'),
    ( 'http://www.liveaboard.com/spirit-of-freedom-liveaboard' , 'AUD'),
    ( 'http://www.liveaboard.com/atlantis-azores' , 'USD'),
    ( 'http://www.liveaboard.com/adora-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/lady-denok-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/princess-dhonkamana' , 'USD'),
    ( 'http://www.liveaboard.com/princess-handy' , 'USD'),
    ( 'http://www.liveaboard.com/manta-mae-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/deep-andaman-queen' , 'USD'),
    ( 'http://www.liveaboard.com/caribbean-explorer-ii' , 'USD'),
    ( 'http://www.liveaboard.com/mantra' , 'EUR'),
    ( 'http://www.liveaboard.com/sachika-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/sea-star-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/thailand-aggressor' , 'USD'),
    ( 'http://www.liveaboard.com/cuan-law-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/moana-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/pindito' , 'USD'),
    ( 'http://www.liveaboard.com/sea-spirit-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/panunee' , 'USD'),
    ( 'http://www.liveaboard.com/halcon' , 'USD'),
    ( 'http://www.liveaboard.com/mv-ari-queen' , 'USD'),
    ( 'http://www.liveaboard.com/duke-of-york-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/kira-kira-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/sea-bird-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/emperor-superior' , 'EUR'),
    ( 'http://www.liveaboard.com/duyung-baru' , 'EUR'),
    ( 'http://www.liveaboard.com/pelagian-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/maldiviana-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/mv-diverace-class-e' , 'USD'),
    ( 'http://www.liveaboard.com/solmar-v' , 'USD'),
    ( 'http://www.liveaboard.com/sea-pearl-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/princess-haleema' , 'EUR'),
    ( 'http://www.liveaboard.com/fun-azul' , 'USD'),
    ( 'http://www.liveaboard.com/theia' , 'USD'),
    ( 'http://www.liveaboard.com/emperor-elite' , 'EUR'),
    ( 'http://www.liveaboard.com/ocean-sapphire-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/amira-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/celebes-explorer' , 'MYR'),
    ( 'http://www.liveaboard.com/emperor-asmaa' , 'EUR'),
    ( 'http://www.liveaboard.com/conte-max' , 'USD'),
    ( 'http://www.liveaboard.com/blue-shark-2' , 'USD'),
    ( 'http://www.liveaboard.com/mv-maldives-princess' , 'USD'),
    ( 'http://www.liveaboard.com/soleil-2' , 'USD'),
    ( 'http://www.liveaboard.com/sea-shell-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/blue-fin-liveaboard' , 'GBP'),
    ( 'http://www.liveaboard.com/sunshine-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/mv-discovery-palawan' , 'USD'),
    ( 'http://www.liveaboard.com/hallelujah-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/the-junk' , 'USD'),
    ( 'http://www.liveaboard.com/my-lucy' , 'USD'),
    ( 'http://www.liveaboard.com/aqua-cat-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/blue-melody' , 'GBP'),
    ( 'http://www.liveaboard.com/giamani' , 'USD'),
    ( 'http://www.liveaboard.com/vision-iii' , 'USD'),
    ( 'http://www.liveaboard.com/moonima-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/mv-similan-explorer' , 'USD'),
    ( 'http://www.liveaboard.com/blue-pearl-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/thai-sea' , 'USD'),
    ( 'http://www.liveaboard.com/miss-moon' , 'USD'),
    ( 'http://www.liveaboard.com/blue-horizon-liveaboard' , 'GBP'),
    ( 'http://www.liveaboard.com/snefro-love' , 'EUR'),
    ( 'http://www.liveaboard.com/red-sea-adventurer' , 'GBP'),
    ( 'http://www.liveaboard.com/my-sheena' , 'EUR'),
    ( 'http://www.liveaboard.com/blackbeards-morning-star' , 'USD'),
    ( 'http://www.liveaboard.com/damai-ii' , 'USD'),
    ( 'http://www.liveaboard.com/my-blue-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/my-seawolf-felo' , 'EUR'),
    ( 'http://www.liveaboard.com/mv-pawara' , 'USD'),
    ( 'http://www.liveaboard.com/mv-marco-polo' , 'USD'),
    ( 'http://www.liveaboard.com/blue-seas-liveaboard' , 'EUR'),
    ( 'http://www.liveaboard.com/snefro-pearl' , 'EUR'),
    ( 'http://www.liveaboard.com/my-seawolf-dominator' , 'EUR'),
    ( 'http://www.liveaboard.com/blackbeards-sea-explorer' , 'USD'),
    ( 'http://www.liveaboard.com/scubapro-i' , 'AUD'),
    ( 'http://www.liveaboard.com/scubapro-ii' , 'AUD'),
    ( 'http://www.liveaboard.com/scubapro-iii' , 'AUD'),
    ( 'http://www.liveaboard.com/manta-queen-7' , 'USD'),
    ( 'http://www.liveaboard.com/sea-serpent' , 'EUR'),
    ( 'http://www.liveaboard.com/snefro-target' , 'EUR'),
    ( 'http://www.liveaboard.com/blue-planet-1' , 'EUR'),
    ( 'http://www.liveaboard.com/manta-queen-6' , 'USD'),
    ( 'http://www.liveaboard.com/snefro-spirit' , 'EUR'),
    ( 'http://www.liveaboard.com/manta-queen-1' , 'USD'),
    ( 'http://www.liveaboard.com/ocean-quest' , 'AUD'),
    ( 'http://www.liveaboard.com/south-moon' , 'EUR'),
    ( 'http://www.liveaboard.com/king-snefro-6' , 'EUR'),
    ( 'http://www.liveaboard.com/grand-sea-serpent' , 'EUR'),
    ( 'http://www.liveaboard.com/obsession' , 'EUR'),
    ( 'http://www.liveaboard.com/my-contessa-mia' , 'EUR'),
    ( 'http://www.liveaboard.com/manta-queen-3' , 'USD'),
    ( 'http://www.liveaboard.com/manta-queen-2' , 'USD'),
    ( 'http://www.liveaboard.com/norseman' , 'EUR'),
    ( 'http://www.liveaboard.com/my-seawolf-soul' , 'EUR'),
    ( 'http://www.liveaboard.com/king-snefro-3' , 'EUR'),
    ( 'http://www.liveaboard.com/manta-queen-5' , 'USD'),
    ( 'http://www.liveaboard.com/my-excellence' , 'EUR'),
    ( 'http://www.liveaboard.com/miss-nouran' , 'EUR'),
    ( 'http://www.liveaboard.com/rocio-del-mar-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/king-snefro-5' , 'EUR'),
    ( 'http://www.liveaboard.com/dreams' , 'EUR'),
    ( 'http://www.liveaboard.com/inula-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/galapagos-aggressor-iii' , 'USD'),
    ( 'http://www.liveaboard.com/mermaid-i' , 'EUR'),
    ( 'http://www.liveaboard.com/damai-i' , 'USD'),
    ( 'http://www.liveaboard.com/blue-manta' , 'USD'),
    ( 'http://www.liveaboard.com/febrina' , 'USD'),
    ( 'http://www.liveaboard.com/pearl-of-papua' , 'USD'),
    ( 'http://www.liveaboard.com/white-manta-indonesia' , 'USD'),
    ( 'http://www.liveaboard.com/odyssey' , 'AUD'),
    ( 'http://www.liveaboard.com/sawasdee-fasai-liveaboard' , 'USD'),
    ( 'http://www.liveaboard.com/mv-discovery-adventure' , 'USD'),
    ( 'http://www.liveaboard.com/sea-hunter' , 'USD')
]

f = open('final.csv', 'w', encoding="utf-8")
writer = csv.writer(f, delimiter='\t')
writer.writerow(['Timestamp', 'URL', 'Vessel name', 'Itinerary name', 'Depature dates', 'Return dates', 'Currency',
                 'Original price', 'Discounted price', 'Discount', 'Notes', 'Availability'])

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

profile = webdriver.FirefoxProfile()
profile.set_preference("network.proxy.type", 1)
profile.set_preference("network.proxy.http", "127.0.0.1")
profile.set_preference("network.proxy.http_port", 8118)
profile.update_preferences()

browser = webdriver.Remote(
    browser_profile=profile,
    command_executor='http://127.0.0.1:4444/wd/hub',
    desired_capabilities=DesiredCapabilities.FIREFOX)
browser.set_page_load_timeout(60)
# driver.manage().timeouts().pageLoadTimeout(maxWaitForPageLoad_seconds, TimeUnit.SECONDS);

whole_data = []

for (link, cur) in vessels:
    logger.info("Current ip is: %s" % get_my_ip())
    logger.info("Link: %s" % link)
    vessel_soup = boil_soup(link)
    ext_dic = {'timestamp' : tmstmp(),
               'url' : link,
               'vessel_name' : get_vessel_name(vessel_soup)}
    vessel_id = get_vessel_id(vessel_soup)
    iti_href = generate_iti_href(vessel_id)

    with elapsed_timer() as elapsed:
        browser.get(link)
        logger.info("Got link: %s (%ss)" % (link, elapsed()))

    time.sleep(1)
    currency_switcher = browser.find_element_by_id('currencySwitcher')
    currency_switcher.click()
    time.sleep(2)
    new_cur = browser.find_element_by_xpath("//a[starts-with(@title,'(%s)')]" % cur)
    new_cur.click()
    time.sleep(1)
    browser.get(iti_href)
    time.sleep(1)
    body = browser.find_element_by_tag_name('body')
    html = body.get_attribute('innerHTML')

    iti_soup = bs(html, 'html.parser')
    itis = scrape_iti_data(iti_soup)
    for iti in itis:
        row = [ext_dic['timestamp'], ext_dic['url'], ext_dic['vessel_name'], iti['iti_name'], iti['start_date'],
               iti['return_date'], cur, iti['price'], iti['disc_price'], iti['disc'], iti['notes'], iti['availability']]
        # logger.info(row)
        writer.writerow(row)

    logger.info("Done with the link")
    time.sleep(3)
    # break

browser.quit()
